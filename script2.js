// №1  Дано число. Проверьте, отрицательное оно или нет. Выведите об этом информацию в консоль.

// function checkNumber(number) {
//     if (number >= 0) {
//         console.log('Число додатнє.');
//     }
//     else {
//         console.log('число від`ємне.');
//     }
// };

// checkNumber(5);
// checkNumber(-3);
// checkNumber(5 - 7);

// done......


// №2 Дана строка. Выведите в консоль длину этой строки.

// function strLength(str) {
//     console.log(str.length)
// };

// strLength('Do you like my owesome roses? ');

// done

// №3 Дана строка. Выведите в консоль последний символ строки.

// let str = ('last symbol');
// console.log(str.charAt(str.length - 1));

// done


// №4 Дано число. Проверьте, четное оно или нет.

// function num(number) {
//     if (number % 2 === 0) {
//         console.log('парне');
//     }
//     else {
//         console.log('не парне');
//     }
// }

// num(3);
// num(5);
// num(7);
// num(6);
// num(2);

// done

// №5 Даны два слова. Проверьте, что первые буквы этих слов совпадают.
// let firstLowerCase;
// let secondLowerCase;
// function checkLetter(firstWord, secondWord) {
//     firstLowerCase = firstWord.toLowerCase();
//     secondLowerCase = secondWord.toLowerCase();
//     if (firstLowerCase[0] === secondLowerCase[0]) {
//         console.log('однакові');
//     }
//     else {
//         console.log('різні');
//     }
// }

// checkLetter('Adelle', 'aligente');
// checkLetter('daddie', 'Frank');

// done

// №6 Дано слово. Получите его последнюю букву. Если слово заканчивается на мягкий знак, то получите предпоследнюю букву.

// function lastLetter(word) {
//     let lastLet = word.charAt(word.length - 1);
//     if (lastLet === ('ь')) {
//         console.log(word.charAt(word.length - 2));
//     } else {
//         console.log(lastLet);
//     }
// };

// lastLetter('міст');
// lastLetter('кінь');

// done

// Уровень 1.2 задачника JavaScript  

// №1  Дано число. Выведите в консоль первую цифру этого числа.


// function firstNum(number) {
// console.log(number.toString().charAt(0));
// };

// firstNum(456);
// firstNum(372);
// firstNum(3455);
// firstNum(234);
// firstNum(9865);

// done


// №2  Дано число. Выведите в консоль последнюю цифру этого числа.

// function lastNumber(number) {
//     let str = number.toString();
//     console.log(str.charAt(str.length - 1));
// };

// lastNumber(53455);
// lastNumber(45678);
// lastNumber(542);

// done

// №3  Дано число. Выведите в консоль сумму первой и последней цифры этого числа.

// function sum(number) {
//     let str = number.toString();
//     let firstNum = (str.charAt(0));
//     if (str.length === 1) {
//         lastNum = 0;
//     } else {
//         lastNum = (str.charAt(str.length - 1))
//     };
//     let summ = parseInt(firstNum) + parseInt(lastNum);
//     console.log(summ);
// }

// sum(9879665);
// sum(234);
// sum(65);
// sum(7);

// done

// №4  Дано число. Выведите количество цифр в этом числе.

// function numberLength(number) {
//     let str = number.toString();
//     console.log(str.length);
// };

// numberLength(5);
// numberLength(3243522);
// numberLength(54);
// numberLength(865);
// numberLength(34);

// done



// №5  Даны два числа. Проверьте, что первые цифры этих чисел совпадают.

// function check(firstNumber, secondNumber) {
//     let firstStr = firstNumber.toString();
//     let secondStr = secondNumber.toString();
//     if (firstStr[0] === secondStr[0]){
//         console.log('співпадають');
//     }
//     else {
//         console.log('не співпадають');
//     }
// };

// check(56, 590);
// check(345, 7645);
// check(764, 78);
// check(234,543);
// check(23, 24);

// done

// №1  Дана строка. Если в этой строке более 
// одного символа, выведите в консоль предпоследний 
// символ этой строки.

// function checkStr(str) {
//     if (str.length > 1) {
//         console.log(str.charAt(str.length - 2));
//     } else {
//         console.log('лише один символ...');
//     }
// };

// checkStr('sdggsd hdfg');
// checkStr('g');
// checkStr('asdda');
// checkStr('5');

// done

// №2  Даны два целых числа. Проверьте, 
// что первое число без остатка делится на второе.

// function check(firstNumber, secondNumber) {
//     if (firstNumber % secondNumber === 0) {
//         console.log('ділиться');
//     } else {
//         console.log('не ділиться');
//     }
// };

// check(4, 2);
// check(5, 3);
// check(8, 4);
// check(6, 4);
// check(9, 3);

// done