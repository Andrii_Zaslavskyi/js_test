// Уровень 1.5 задачника JavaScript
// №1  Найдите сумму всех целых чисел от 1 до 100.


// let summ = 0;
// function sum(number) {
//     for (let i = 1; i < number; i++) {
//         summ = summ + (i + 1);
//         console.log(summ);
//     };
// };

// sum(100);

// done


// №2  Найдите сумму всех целых четных чисел в 
// промежутке от 1 до 100.


// let summ = 0;
// function sum(number) {
//     for (let i = 1; i < number; i++) {
//         if (i % 2 === 0) {
//             summ = summ + i;
//         }
//         console.log(summ);
//     };
// };

// sum(100);

// done



// №3  Найдите сумму всех целых нечетных 
// чисел в промежутке от 1 до 100.

// let summ = 0;
// function sum(number) {
//     for (let i = 1; i < number; i++) {
//         if (i % 2 !== 0) {
//             summ = summ + i;
//         }
//         console.log(summ);
//     };
// };

// sum(100);

// done


// №4  Даны два целых числа. Найдите 
// остаток от деления первого числа на второе.

// function x(firstNumber, secondNumber) {
//     let remainder = firstNumber % secondNumber;
//     console.log(remainder);
// }

// x(4, 2);
// x(6, 3);
// x(7, 4);


// done

// №5  Дана некоторая строка. Переберите и выведите 
// в консоль по очереди все символы с конца строки.



// function reverse(str) {
//     let newStr = ('');
//     for (let i = str.length - 1; i >= 0; i--) {
//         newStr = newStr.concat(str.charAt(i));
//     };
//     console.log(newStr);
// };


// reverse('Аманогава');
// reverse('Task');
// reverse('Karolina');
// reverse('groove');

// done